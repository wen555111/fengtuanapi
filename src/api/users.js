import request from "../utils/request";
import qs from "qs";

export const getUsersapi = (params) => {
  return request({
    url: "users/index.jsp",
    method: "get",
    params: qs.stringify(params),
  });
};
export const postUsersapi = (params) => {
  return request({
    url: "users/create.jsp",
    method: "post",
    data: qs.stringify(params),
  });
};
export const putUsersapi = (params) => {
  return request({
    url: "users/update.jsp",
    method: "put",
    data: qs.stringify(params),
  });
};
export const deleteUsersapi = (params) => {
  return request({
    url: "users/delete.jsp",
    method: "delete",
    params: qs.stringify(params),
  });
};
