import request from "../utils/request";
import qs from "qs";

export const postLoginSmsapi = (params) => {
  return request({
    url: "users/login.jsp",
    method: "post",
    data: qs.stringify(params),
  });
};
