// 导入样式
import { Container } from "./create.styled";
// 导入API
import { useNavigate } from "react-router-dom";
import { postUsersapi } from "../../api/users";
// 导入组件
import { Card, Form, Input, Select, Button, message } from "antd";
const { Option } = Select;

function UsersCreate() {
  const navigate = useNavigate();

  // 表单登录
  const onFinish = async (values) => {
    console.log("Success:", values);
    let res = await postUsersapi(values);
    if (res.meta.state === 201) {
      message.success(res.meta.msg);
      navigate("/admin/users");
    } else {
      message.error(res.meta.msg);
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <Container>
      <Card
        title="用户创建"
        extra={
          <Button type="primary" onClick={() => navigate("/admin/users")}>
            返回
          </Button>
        }
      >
        <Form onFinish={onFinish} onFinishFailed={onFinishFailed}>
          <Form.Item
            name="question"
            rules={[
              {
                required: true,
                message: "请选择密保问题",
              },
            ]}
          >
            <Select placeholder="请选择密保问题">
              <Option value="你父亲的名字">你父亲的名字</Option>
              <Option value="您其中一位老师的名字">您其中一位老师的名字</Option>
              <Option value="你奶奶的名字">你奶奶的名字</Option>
            </Select>
          </Form.Item>
          <Form.Item
            name="answer"
            rules={[
              {
                required: true,
                message: "请选择密保问题",
              },
            ]}
          >
            <Input placeholder="请输入密保答案" />
          </Form.Item>
          <Form.Item
            name="mobile"
            rules={[
              {
                validator: (_, value) => {
                  if (!value) return Promise.reject(new Error("请输入手机号"));

                  if (!/^1\d{10}$/.test(value)) {
                    return Promise.reject(new Error("请输入正确的手机号"));
                  }
                  return Promise.resolve();
                },
              },
            ]}
          >
            <Input placeholder="请输入手机号" />
          </Form.Item>
          <Form.Item
            name="username"
            rules={[
              {
                validator: (_, value) => {
                  if (!value) return Promise.reject(new Error("请输入用户名"));

                  if (value.length < 2 || value.length > 10) {
                    return Promise.reject(new Error("用户名只能2~10个字符"));
                  }
                  return Promise.resolve();
                },
              },
            ]}
          >
            <Input placeholder="请输入用户名" />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: "请输入密码",
              },
            ]}
          >
            <Input placeholder="请输入密码" type="password" />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              创建
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </Container>
  );
}

export default UsersCreate;
