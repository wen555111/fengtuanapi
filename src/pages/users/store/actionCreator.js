import { getUsersapi } from "../../../api/users";

export const getUsersAction = (params) => {
  return async (dispatch) => {
    let res = await getUsersapi(params);
    res.data.total = parseInt(res.data.total);
    dispatch({
      type: "USERS/SET_TABLEDATA",
      payload: res.data,
    });
  };
};
