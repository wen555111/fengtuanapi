// 导入样式
import { Container } from "./styled";
// 导入API
import { useNavigate } from "react-router-dom";
import { getUsersAction } from "./store/actionCreator";
import { connect } from "react-redux";
import { deleteUsersapi } from "../../api/users";
// 导入组件
import UserEdit from "./components/userEdit";
import {
  FormOutlined,
  DeleteOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import {
  Card,
  Button,
  Input,
  DatePicker,
  Table,
  Pagination,
  Modal,
  message,
} from "antd";
import { useEffect, useState } from "react";
const { Search } = Input;
const { RangePicker } = DatePicker;

const onChange = (value, dateString) => {
  console.log("Selected Time: ", value);
  console.log("Formatted Selected Time: ", dateString);
};

const onOk = (value) => {
  console.log("onOk: ", value);
};

const itemRender = (_, type, originalElement) => {
  return originalElement;
};

function Users(props) {
  let [userEdit, setUserEdit] = useState(false);
  let [userEditRow, setUserEditRow] = useState({});
  const handleDel = (row) => {
    console.log(row);
    Modal.confirm({
      title: "你确定要删除吗?",
      icon: <ExclamationCircleOutlined />,
      content: "删除后无法找回！！！🐳",

      async onOk() {
        console.log("OK");
        let params = { user_id: row.user_id };
        let res = await deleteUsersapi(params);
        if (res.meta.state === 200) {
          message.success(res.meta.msg);
          props.handleInitData(params);
        }
      },

      onCancel() {
        console.log("Cancel");
      },
    });
  };
  // 表格列
  const columns = [
    {
      title: "编号",
      dataIndex: "user_id",
      key: "user_id",
    },
    {
      title: "姓名",
      dataIndex: "username",
      key: "username",
    },
    {
      title: "角色名",
      dataIndex: "role_name",
      key: "role_name",
    },

    {
      title: "操作",
      key: "operation",
      fixed: "right",
      width: 150,
      render: (text, record, index) => {
        // console.log(text, record, index);
        return (
          <>
            <Button
              style={{ marginRight: "10px" }}
              onClick={() => {
                console.log(text, record, index);
                setUserEdit(true);
                setUserEditRow(record);
              }}
            >
              <FormOutlined />
            </Button>
            <Button onClick={() => handleDel(record)}>
              <DeleteOutlined />
            </Button>
          </>
        );
      },
    },
  ];

  const onSearch = (value) => {
    console.log(value);
  };
  const navigate = useNavigate();

  let [params, setParams] = useState({
    pagenum: 1,
    pagesize: 10,
  });
  useEffect(() => {
    // eslint-disable-next-line no-undef
    props.handleInitData(params);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [params.pagenum]);

  const onPage = (page, pageSize) => {
    console.log(page, pageSize);
    setParams({
      ...params,
      pagenum: page,
    });
  };

  return (
    <Container>
      <UserEdit
        state={userEdit}
        row={userEditRow}
        close={() => setUserEdit(false)}
      />
      <Card
        title="用户列表"
        extra={
          <Button
            type="primary"
            onClick={() => navigate("/admin/users/create")}
          >
            创建
          </Button>
        }
      >
        {/* 筛选 */}
        <div className="filter">
          <Search
            placeholder="请输入用户名"
            onSearch={onSearch}
            enterButton
            style={{ width: "300px" }}
          />
          &nbsp;&nbsp;
          <RangePicker
            showTime={{ format: "HH:mm" }}
            format="YYYY-MM-DD HH:mm"
            onChange={onChange}
            onOk={onOk}
          />
        </div>
        {/* 筛选 */}

        {/* 表格 */}
        <Table
          dataSource={props.tableData.list}
          columns={columns}
          pagination={false}
          rowKey="user_id"
        />
        {/* 表格 */}

        {/* 分页 */}
        <Pagination onChange={onPage} total={500} itemRender={itemRender} />
        {/* 分页 */}
      </Card>
    </Container>
  );
}
const mapStateToProps = (state) => {
  console.log("拿到数据：", state.toJS());
  return {
    tableData: state.toJS().users.tableData,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    handleInitData: (params) => dispatch(getUsersAction(params)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Users);
