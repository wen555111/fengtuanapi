import styled from "styled-components";

export const Container = styled.div`
  .ant-table {
    margin: 20px 0;
  }

  .ant-pagination {
    width: 100%;
    text-align: center;
  }
`;
