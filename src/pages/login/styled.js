import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  height: 100%;
  background-color: #2d3a4b;

  display: flex;
  justify-content: center;
  align-items: center;

  .ant-form {
    width: 300px;

    h1 {
      font-size: 30px;
      color: #fff;
      text-align: center;
      font-weight: bold;
    }

    .ant-select .ant-select-selector {
      height: 40px;
      padding-top: 5px;
    }
    .ant-input {
      height: 40px;
    }

    button {
      width: 100%;
    }
  }
`;
